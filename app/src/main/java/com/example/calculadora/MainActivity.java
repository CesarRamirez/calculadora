package com.example.calculadora;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnSuma;
    Button btnResta;
    Button btnMult;
    Button btnDivi;
    Button btnCerrar;
    Button btnLimpiar;

    float res;

    EditText txtNum1;
    EditText txtNum2;
    EditText txtRes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        btnSuma = (Button) findViewById(R.id.btnSuma);
        btnResta = (Button) findViewById(R.id.btnResta);
        btnDivi = (Button) findViewById(R.id.btnDivi);
        btnMult = (Button) findViewById(R.id.btnMult);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnCerrar = (Button) findViewById(R.id.btnCerrar);

       txtNum1 = (EditText) findViewById(R.id.txtNum1);
       txtNum2 = (EditText) findViewById(R.id.txtNum2);
       txtRes = (EditText) findViewById(R.id.txtRes);

       btnSuma.setOnClickListener(this);
       btnResta.setOnClickListener(this);
       btnDivi.setOnClickListener(this);
       btnMult.setOnClickListener(this);
       btnLimpiar.setOnClickListener(this);
       btnCerrar.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        Operaciones obj = new Operaciones();
        obj.setNum1(Float.parseFloat(txtNum1.getText().toString()));
        obj.setNum2(Float.parseFloat(txtNum2.getText().toString()));


        if (btnSuma == v){
            res = obj.suma();
            txtRes.setText(""+res);

        }else if(btnResta == v){
            res = obj.resta();
            txtRes.setText(""+res);

        }else if(btnMult == v){

            res = obj.mult();
            txtRes.setText(""+res);
        }else if(btnDivi == v){
            res = obj.div();
            txtRes.setText(""+res);
        }else if(btnLimpiar == v){
            limpiar();
        }else if(btnCerrar == v){
            finish();
        }
    }

    public void limpiar() {
        txtNum1.setText("0");
        txtNum2.setText("0");
        txtRes.setText("0");

    }


}
